import { PrestadoresPage } from './../prestadores/prestadores';
import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { ListPage } from '../../pages/list/list';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  prestadores: any = PrestadoresPage;
  visitantes: any = ListPage;
  historico: any = ListPage;

  constructor(public navCtrl: NavController) {

  }

}
